package com.thanawuth;

public class Tree {
    private String name;
    private int x;
    private int y;
    
    public Tree(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;

    }
    public void print() {
        System.out.println(name + " x:" + x + " y:" + y);
    }

    public String getName() {
        return name;
    }
}
