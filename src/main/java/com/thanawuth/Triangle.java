package com.thanawuth;

public class Triangle {
    private double a;
    private double b;
    private double c;
    private double s;

    
    
    public Triangle(double a, double b, double c) {
       this.a = a;
       this.b = b;
       this.c = c;
    }
    public double getArea() {
        s = (a + b + c)/2;
        return  Math.sqrt(s*(s-a)*(s-b)*(s-c));
    }

    public double getPerimeter() {
        return a + b + c;
    }

}
